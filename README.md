# AME 220, Spring 2019


## References/Links

* BASH basics:
https://www.tldp.org/LDP/Bash-Beginners-Guide/html/
(look through chapter 1-4 for sure).
* GIT basics:
https://git-scm.com/book/en/v2/Getting-Started-The-Command-Line
* HTML tutorial:
https://www.w3schools.com/html/
* CSS Tutorial:
https://www.w3schools.com/css/
